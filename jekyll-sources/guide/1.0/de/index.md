---
layout: guide/1_0
title: "Belanz 1.0 Kurzanleitung"
html_lang: "de"

navbar:
  options_bar: "Einstellungen"
  company_info: "Firma"
  assets: "Vermögen"
  journal: "Buchungen"
  report: "Abschluss"

intro:
  image_alt: "Firmendaten-Bereich eines Belanz-Dokuments"
  text1: "Willkommen in der Kurzanleitung von Belanz, einer dokument&shy;basierten Buchhaltungs-App für macOS, die Kleinst&shy;kapital&shy;gesell&shy;schaften beim Kontieren und erstellen ihrer Jahres&shy;abschlüsse helfen kann."
  text2: "Ein Belanz-Dokument (in einem Tab oder eigenem Fenster) besteht aus folgenden Bereichen: Die <i>Options&shy;leiste</i>, in der man das Geschäfts&shy;jahr sowie andere Para&shy;meter, die alle Bereiche betreffen, einstellt; die Bereiche <i>Firmendaten</i>, <i>Vermögen</i> und <i>Buchungen</i>, in denen Buchhaltungs&shy;daten eines Unter&shy;nehmens für das Geschäfts&shy;jahr eingetragen werden; und schließlich der Bereich <i>Abschluss</i>, in dem man aus den ein&shy;ge&shy;tragenen Daten einen Jahres&shy;abschluss erstellen kann. Die Options&shy;leiste ist ständig sichtbar, von den anderen Bereichen ist jeweils nur einer sichtbar. Den Arbeits&shy;bereich schaltet man entweder in der Werkzeug&shy;leiste des Fensters oder über die Tasten&shy;kombination um."

options_bar:
  title: "Dokumentoptionen einstellen"
  image_alt: "Werkzeug- und Optionsleiste zweier Belanz-Dokumentfenster"
  text: "Die Optionsleiste, unterhalb der Werkzeug&shy;leiste des Fensters, bietet direkten Zugriff auf Einstellungen die von mehreren Abschnitten benutzt werden. Von links nach rechts:"
  item1:
    title: "Geschäftsperiode (Geschäftsjahr):"
    text: "Üblicherweise entspricht das Geschäfts&shy;jahr dem Kalender&shy;jahr, vom 1. Januar bis 31. Dezember, abgesehen vom Gründungs- oder Auflösungs&shy;jahr."
  item2:
    title: "Die XBRL-Taxonomie:"
    text: "Die anwendbare Taxonomie hängt vom Stichtag des Jahres&shy;abschlusses und wird automatisch auf eine geeignete Version gesetzt. Eine alternative Taxonomie, falls vorhanden, kann man hier auswählen."
  item3:
    title: "Der Kontenrahmen:"
    text: "The Auswahl&shy;möglichkeiten sind; DATEV Standard&shy;kontenrahmen 3 (SKR03), Industrie&shy;kontenrahmen (IKR) oder XBRL DE-GAAP-Konten. DE-GAAP-Konten sind der Haupt&shy;kontenrahmen von Belanz. SKR03- und IKR-Kontent werden für die Verarbeitung in DE-GAAP-Kontent umgewandelt."
  item4:
    title: "Die Art der Abschlussbilanz:"
    text: "Nach welchem rechtlichen Rahmen bilanziert wird. Belanz unterstützt zwar nur Einheits&shy;bilanzen, fügt aber bei Auswahl der Steuer&shy;bilanz dem Jahres&shy;abschluss einen Anlagespiegel hinzu."
  item5:
    title: "Der Dateipfad zum Vorjahresdokument:"
    text: "Das Vorjahresdokument ist das Belanz-Dokument für das Vorjahr. Die Abschluss&shy;daten des Vorjahres werden benötigt um die Anfangs&shy;werte für die Konten des aktuellen Geschäfts&shy;jahres zu setzen. Drücken Sie den Knopf mit dem Verzeichnis-Symbol um im Datei&shy;system nach dem Dokument zu suchen. Der Pfad kann auch manuell eingegeben werden wobei relative Pfade (relativ zum Verzeichnis des aktuellen Dokuments) unterstützt werden. Ist der Datei&shy;pfad gesetzt und das Dokument erfolgreich geladen, kann man durch drücken auf das Auge-Symbol den Vorjahre&shy;sabschluss ein- und ausblenden."

company_info:
  title: "Firmendaten eingeben"
  image_alt: "Geschäftsführer-Eingabefelder im Firmendaten-Bereichs eines Belanz-Dokuments"
  text: "Im Firmen&shy;datenbereich gibt man die für einen Abschluss relevanten und für den Stichtag des Abschlusses gültigen Basis&shy;daten des Unter&shy;nehmens ein. Dies sind, unter anderem, der Firmen&shy;name, der Firmen&shy;sitz, die Steuer&shy;nummer, die Rechts&shy;form, die steuer&shy;lichen Daten der Gesell&shy;schafter, sowie die Kontakt&shy;daten des Geschäfts&shy;führers.<br/><br/>Wurde noch keine Änderung in einem der Felder gemacht und ist ein Belanz-Vorjahres&shy;dokument gesetzt, wird Belanz vorschlagen, die Werte aus dem Vorjahres&shy;dokument zu übernehmen."

assets:
  title: "Firmenvermögen eingeben"
  image_alt: "ein Anlagevermögensposten im Vermögens-Bereich eines Belanz-Dokuments"
  debts:
    image_alt: "ein Schuldenposten im Vermögens-Bereich eines Belanz-Dokuments"
  text: "Anlagevermögen, Umlaufvermögen und Schulden der Firma werden im Vermögens&shy;bereich eingetragen.<br/><br/>Anlagevermögen werden nach Typ (Gebäude, technische Anlagen, Geschäfts&shy;ausstattung usw.) und Inventar&shy;nummer sortiert und können mit einem  Abschreibungs&shy;plan versehen werden. Umlauf&shy;vermögen haben keine Inventar&shy;nummer und werden nach Typ und Wert sortiert. Schulden bestehen aus Darlehen und Rück&shy;zahlungen und werden nach letztem Rück&shy;zahlungs&shy;termin und dem Gesamtwert sortiert."

journal:
  title: "Buchungen eintragen"
  image_alt: "Buchungs-Bereich eines Belanz-Dokuments"
  text1: "Der Buchungs&shy;bereich enthält alle Buchungen der Firma für das aktuelle Geschäfts&shy;jahr. Buchungen können hinzugefügt, verändert, dupliziert oder entfernt werden. Eine Buchung kann mit kurzen Text&shy;markierungen (Tags) versehen werden um, zum Beispiel, die Buchung mit einem Anlage&shy;vermögen zu assozieren. Buchungen können durch Eingabe eines Such&shy;wortes im Suchfeld der Fenster&shy;leiste gefiltert werden. Es werden dann nur die Buchungen angezeigt in deren Titel oder Markierungen das Suchwort vorkommt."
  text2: "Jede neue Buchung enthält einen leeren Titel, einen leeren Soll-Eintrag und einen leeren Haben-Eintrag. Sie vervollständigen die Buchung indem Sie die leeren Felder ausfüllen. Falls nötig, können weitere Soll- oder Haben-Einträge hinzugefügt werden.<br/><br/>Belanz nutzt intern DE-GAAP-Konten um den Abschluss zu erstellen. Nicht alle SKR03- und IKR-Konten können eindeutig einem DE-GAAP-Konto zugewiesen werden. Daher wird nur eine Unter&shy;menge der SKR03- und IKR-Kontent unterstützt."

report:
  title: "Jahresabschluss erstellen und exportieren"
  image_alt: "Abschlussbereich eines Belanz-Dokuments"
  text1: "Im Abschlussbereich kann durch drücken des \"Erstellen\"-Knopfes in der Kontroll&shy;leiste ein Jahres&shy;abschluss erstellt werden. Ein Belanz-Jahres&shy;abschluss enthält u. a. die Berichtsteile Bilanz und Gewinn- und Verlust&shy;rechnung. Durch den Umschalter im Abschluss&shy;bereich kann man sich die den Abschluss als Ganzes oder ausgewählte Berichts&shy;teile ansehen.<br/><br/>Unternehmen sind verpflichtet einen auf Papier ausgedruckten und durch Unterschrift gebilligten Jahres&shy;abschluss aufzubewahren. Um dies zu ermöglichen bietet Belanz den Export des Jahres&shy;abschlusses als <a href=\"https://www.latex-project.org\">LaTeX</a>-Dokument, aus dem man anschließend ein PDF erstellen kann.<br/><br/>Zusätzlich sind Unternehmen verpflichtet den Jahres&shy;abschluss digital, im XBRL-Format, an die Steuer&shy;behörde zu übertragen. Dies wird als E-Bilanz bezeichnet. Belanz unterstützt die E-Bilanz nicht direkt, kann die Daten aber als <a href=\"https://www.myebilanz.de\">myebilanz</a>-INI-Datei exportieren.<br/><br/>Die Export-Optionen finden Sie in einem Menü in der Werkzeug&shy;leiste des Fensters."
  text2: "Der Jahres&shy;abschluss kann wiederholt erstellt werden. Die bestehende Version wird dabei von der neuen Version ersetzt.  Durch drücken des roten Mülleimer-Knopfes, neben dem \"Erstellen\"-Knopf, kann der Jahres&shy;abschluss auch ohne Ersatz gelöscht werden."
---
