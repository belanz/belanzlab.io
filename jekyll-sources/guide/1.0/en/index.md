---
layout: guide/1_0
title: "Getting Started with Belanz 1.0"
html_lang: "en"

navbar:
  options_bar: "Settings"
  company_info: "Company"
  assets: "Assets"
  journal: "Journal"
  report: "Report"

intro:
  image_alt: "the company section of a Belanz document"
  text1: "Welcome to a quick introduction to Belanz, a document-based macOS application that can help micro-corporations in Germany do their accounting and generate financial statements."
  text2: "A Belanz document, either in a tab or in its own window, has various sections: The <i>Options Bar</i>, where you set the business period of your company, as well as other document-wide options; the <i>Company</i>, <i>Assets</i>, and <i>Journal</i> sections, where you enter the accounting data for the business period of your company; and, lastly, the <i>Report</i> section, where you generate financial statements. The Options Bar is always visible, the other sections are only visible one at a time. You can switch between these sections using keyboard shortcuts or the section switcher in the window toolbar."

options_bar:
  title: "Document settings"
  image_alt: "toolbar and options bar of two document windows"
  text: "The options bar, located under the toolbar of the window, is where you set general attributes that are shared between sections. These are, from left to right:"
  item1:
    title: "The business period:"
    text: "Usually corresponds to the calendar year, January 1st to December 31st, unless it's a founding year or a liquidation year."
  item2:
    title: "The XBRL taxonomy:"
    text: "The applicable taxonomy depends on the business period end date and is set automatically, after entering the business period. If an alternative taxonomy is available, you can select it here."
  item3:
    title: "The accounts chart:"
    text: "The choices are; DATEV Standard&shy;konten&shy;rahmen 03 (SKR03), Industrie&shy;konten&shy;rahmen (IKR), or XBRL DE-GAAP accounts, where SKR03 and IKR are mapped to the natively used DE-GAAP accounts."
  item4:
    title: "The report type:"
    text: "The types of business reports that can be generated are <i>trade</i> and <i>fiscal</i>. Belanz actually calculates the balance sheet account totals based on unified (trade and fiscal) accounts only. Selecting the fiscal type, however, will add an asset changes statement to the report."
  item5:
    title: "The file path to the base document:"
    text: "The base document is a Belanz document for the previous business period that contains report data. The base report is required for the correct initialization of the acccounts of the current business period. Use the folder-shaped button to set the file location via file browser. You can also manually enter an absolute or relative file path (relative to the folder containing the current document) to the base document. If the report from the base document did load successfully, you can view it by clicking the eye-shaped button."

company_info:
  title: "Entering the company information"
  image_alt: "CEO entry fields in the company section of a Belanz document"
  text: "In the Company section, you enter the company's fundamental data as is current at the end date of the business period. This includes, for example, the company name, the company seat, its tax number, its legal form, tax information of the shareholders, and the contact information of the chief executive officers.<br/><br/>If you haven't edited any fields yet, and a Belanz base document for the previous business period is set, Belanz will offer filling in the fields with the values from the base document."

assets:
  title: "Adding assets and debts"
  image_alt: "a fixed asset item in the assets section of a Belanz document"
  debts:
    image_alt: "a debt item in the assets section of a Belanz document"
  text: "In the <i>Assets</i> section you enter the company's fixed assets, current assets and debts.<br/><br/>Fixed assets can have a depreciation schedule and are sorted by category (building, machinery, office equipment etc.), and by registration ID. Current assets have no registration ID and are sorted by category and value. Debts consist of loans and back payments, and are sorted by their final payment date as well as total value."

journal:
  title: "Adding journal records"
  image_alt: "journal section of a Belanz document"
  text1: "The Journal section lists all records in the company's bookkeeping journal for the current period. Records can be added, edited, duplicated, or removed. Records can be tagged with arbitrary text tokens that, for example, associate a record with a fixed asset. Records can be filtered by entering a search key into the search field in the toolbar. The list then shows only those records where the search key occurs either in the title or in a tag."
  text2: "Each new record contains an empty title, one empty debit entry and one empty credit entry. You complete the record by providing the title, as well as the account name and value for each entry, adding more entries of either type as needed.<br/><br/>Belanz works natively with DE-GAAP accounts. SKR03 and IKR accounts are internally converted to the DE-GAAP accounts. A one-to-one conversion of all accounts is not possible. So, only a subset of SKR03 and IKR accounts are supported."

report:
  title: "Generating and exporting reports"
  image_alt: "report section of a Belanz document"
  text1: "In the Report section, you can generate a Belanz report by clicking on the \"Generate\" button in the <i>control bar</i> at the top. A Belanz report is basically a financial statement, including balance sheet, income statement and more. You can view the generated report as a whole or by individual statement using the selector at the top.<br/><br/>Companies are required to keep a signed paper copy of the financial statement in their records. For this purpose, you can export the generated report as a <a href=\"https://www.latex-project.org\">LaTeX</a> document, from which you can then create a PDF.<br/><br/>Companies are also required to digitally submit the fiscal financial statement to the tax authority in XBRL format, referred to as E-Bilanz. Belanz does not yet support the E-Bilanz method directly but can export the data to a <a href=\"https://www.myebilanz.de\">myebilanz</a> INI file.<br/><br/>The report export options are available in the export menu located in the window toolbar."
  text2: "You can generate a report repeatedly, replacing the existing report. You can also delete the existing report without generating a new one by clicking on the red trash can symbol in the control bar, next to the generator button."
---
