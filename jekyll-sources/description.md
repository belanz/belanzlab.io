---
layout: default
title: Beschreibung
---

## Der Jahresabschluss mit Belanz

Der Jahresabschluss wird erstellt nach den im Handelsgesetzbuch ausgewiesenen Vereinfachungen für Kleinstkapitalgesellschaften.

Belanz ist nicht geeignet für kapitalmarktorientierte Kapitalgesellschaft, Aktiengesellschaft oder Gesellschaften die Teil eines Konzerns sind.

### Allgemeine Angaben

Laut [§ 264 Abs. 1a HGB](https://www.gesetze-im-internet.de/hgb/__264.html) sind in dem
Jahresabschluss die Firma, der Sitz, das Registergericht und die Nummer, unter der die
Gesellschaft in das Handelsregister eingetragen ist, anzugeben. Falls sich die Gesellschaft
in Liquidation oder Abwicklung befindet, ist dies anzugeben.

### Inventar

Die Jahresschlussbilanz fängt an mit der Aufstellung eines Inventars gemäss
[§ 240 HGB](https://www.gesetze-im-internet.de/hgb/__240.html). Im Inventar aufzuzeichnen sind
* Grundstücke
* Forderungen
* Schulden
* Bargeld
* Sonstige Vermögensgegenstände

### Gewinn- und Verlustrechnung (GuV)

Für Kleinstkapitalgesellschaften kann nach [§ 275 Abs. 5 HGB](https://www.gesetze-im-internet.de/hgb/__275.html)
folgende vereinfachter Struktur verwendet werden:

1. Umsatzerlöse
2. sonstige Erträge
3. Materialaufwand
4. Personalaufwand
5. Abschreibungen
6. sonstige Aufwendungen
7. Steuern
8. Jahresüberschuss/Jahresfehlbetrag

### Bilanz


### Anhänge

Die gesetzlichen Vertreter einer Kapitalgesellschaft haben den Jahresabschluß
um einen Anhang zu erweitern, der mit der Bilanz und der Gewinn- und Verlustrechnung
eine Einheit bildet, sowie einen Lagebericht aufzustellen
[(§ 264 Abs. 1 HGB)](https://www.gesetze-im-internet.de/hgb/__264.html).

Kleinstkapitalgesellschaften ([§ 267a HGB](https://www.gesetze-im-internet.de/hgb/__267a.html))
sind jedoch davon ausgenommen wenn unter der Bilanz folgende Angaben gemacht werden:

    A) Für die in unten bezeichneten Haftungsverhältnisse sind
    1. die Angaben zu nicht auf der Passivseite auszuweisenden Verbindlichkeiten und Haftungsverhältnissen im Anhang zu machen,
    2. dabei die Haftungsverhältnisse jeweils gesondert unter Angabe der gewährten Pfandrechte und sonstigen Sicherheiten anzugeben und
    3. dabei Verpflichtungen betreffend die Altersversorgung und Verpflichtungen gegenüber verbundenen oder assoziierten Unternehmen jeweils gesondert zu vermerken.

    "Unter der Bilanz sind, sofern sie nicht auf der Passivseite auszuweisen sind,
        Verbindlichkeiten aus der Begebung und Übertragung von Wechseln, aus Bürgschaften,
        Wechsel- und Scheckbürgschaften und aus Gewährleistungsverträgen sowie
        Haftungsverhältnisse aus der Bestellung von Sicherheiten für fremde Verbindlichkeiten
        zu vermerken; sie dürfen in einem Betrag angegeben werden. Haftungsverhältnisse sind
        auch anzugeben, wenn ihnen gleichwertige Rückgriffsforderungen gegenüberstehen."

    B) für die Mitglieder des Geschäftsführungsorgans, eines Aufsichtsrats, eines Beirats oder einer ähnlichen Einrichtung jeweils für jede Personengruppe
        die gewährten Vorschüsse und Kredite unter Angabe der Zinssätze, der wesentlichen Bedingungen und der gegebenenfalls im Geschäftsjahr zurückgezahlten oder erlassenen Beträge sowie die zugunsten dieser Personen eingegangenen Haftungsverhältnisse;

### Prüfung

Kleinstkapitalgesellschaften benötigen für die Erstellung des Jahresabschlusses keinen Prüfer
(siehe [§ 316 Abs. 1 HGB](https://www.gesetze-im-internet.de/hgb/__316.html)).

## Anwendung

### Kontenrahmen

Ein Kontenrahmen ist eine umfangreiche Sammlung von eindeutig nummerierten Buchungskonten.
Ein Standardkontenrahmen ist ein Kontenrahmen der von vielen Firmen benutzt wird.
Die meisten Firmen in Deutschland verwenden die [von der DATEV herausgegebenen Standardkontenrahmen SKR 03 oder SKR 04](https://www.datev.de/web/de/m/ueber-datev/datev-im-web/datev-von-a-z/skr-standard-kontenrahmen/).
Diese Software bietet die Möglichkeit, die SKR03-Nummer für den Zugriff auf Konten zu benutzen.

### Kontenplan

Der Kontenplan einer Firma ist eine engere Auswahl der Konten aus dem Kontenrahmen.
Dies sind die Konten die in der Buchhaltung der Firma tatsächlich benutzt werden.

### e-Bilanz

Die e-Bilanz ist ein vom Bundesfinanzamt vorgeschriebenes Verfahren für die elektronische Meldung
von Bilanzen deutscher Unternehmen. Die Datenstruktur, die verwendet wird um die Bilanz von Kapitalgesellschaften
an das Finanzamt zu übertragen, ist eine an das Handelsgesetzbuch angepasste Version des XBRL-Dateiformates
(E**x**tensible **B**usiness **R**eporting **L**anguage).

## Hintergrund der App

Die App ist aus eigenem Bedarf entstanden. Bestehende Softwareangebote haben
entweder funktional nicht zu meinem Anwendungsfall (Kleinstkapitalgesellschaft)
gepasst oder die Benutzerschnittstelle hat mir nicht gefallen. Zudem sehe ich
dringenden Bedarf für eine Lösung mit offenem Quellcode als Baustein für die
komplette Automatisierung der Buchhaltung.
