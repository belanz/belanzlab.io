---
layout: privacy
title:
  de: "Datenschutzerklärungen"
  en: "Privacy Statements"
---

# Datenschutzerklärungen

<br />

## 1. Datenschutzerklärungen zum Webauftritt

Diese Daten&shy;schutz&shy;erklärung soll die Nutzer des Web&shy;auftrittes (Website) <i>belanz.gitlab.io</i> über die Art, den Umfang und den Zweck der Erhebung und Verwendung personen&shy;bezogener Daten informieren.

Dieser Web&shy;auftritt ist durch die Nutzung des Dienstes [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) entstanden. Dadurch teilt sich die Verant&shy;wortlich&shy;keit für den Umgang mit personen&shy;bezogenen Daten auf zwischen Web&shy;dienst-Anbieter und dem inhaltlich Verant&shy;wortlichen.

<br />

### 1.1. Datenschutzerklärung des Webdienst-Anbieters

Der Web&shy;dienst, über den besuchende Web-Browser Zugriff auf den Inhalt des Web&shy;auftrittes (belanz.gitlab.io) erhalten, liegt in der Verant&shy;wortlich&shy;keit der Firma [GitLab](https://gitlab.com). Als "Inhalt des Webauftritts" sind alle über das HTTP-Protokoll von der Internet-Adresse <i>belanz.gitlab.io</i> an die IP-Adresse des Nutzers über&shy;tragenen Daten zu verstehen. Als "Webdienst" ist die Hardware- und Software-Infrastruktur zu verstehen, die Zugriffe auf den Inhalt des Web&shy;auftritts mittels HTTP-Protokoll möglich macht und die Parameter der Daten&shy;verbindung kontrolliert. Die Firma GitLab hat somit Zugang zu personen&shy;bezogenen Daten wie, z.B., die IP-Adresse des Web-Browsers. Diese für GitLab zugäng&shy;lichen Parameter der Daten&shy;verbindung sind für die vom übertragenen Inhalt Verant&shy;wortlichen nicht zugänglich.

Die Daten&shy;schutz&shy;erklärung sowie Anschrift von GitLab finden Sie auf [https://about.gitlab.com/privacy](https://about.gitlab.com/privacy/) bzw. [https://about.gitlab.com/company/visiting](https://about.gitlab.com/company/visiting/)

<br />

### 1.2. Datenschutzerklärung des inhaltlich Verantwortlichen

Der Inhalt des Web&shy;auftrittes ist rein statisch, enthält also keinen Programm&shy;code (JavaScript). Das bedeutet, der geladene Inhalt ist nicht fähig während der Anzeige im Browser personen&shy;bezogene Daten (z.B. die IP-Adresse) zu sammeln. Es werden auch keine Daten im Browser hinterlegt (sogenannte Cookies), welche Spuren vom Besuch des Browsers auf diesem Web&shy;auftritt hinterlassen und somit für die Verfolgung der Web&shy;aktivität von Nutzern misbraucht werden können (sogenanntes Tracking).

Der Inhalt des Web&shy;auftrittes enthält keine Verknüpf&shy;ungen zu Dritten, die beim Besuch automatisch aktiviert werden. Die einzigen Verknüpf&shy;ungen zu Inhalten von Dritten sind im Inhalt als solche zu erkennen, müssen im Browser vom Benutzer bewusst aktiviert werden und führen zum Verlassen des Web&shy;auftritts. Das bedeutet, Ihr Besuch wird nicht von Werbe&shy;diensten, Analyze-Tools oder Social-Media-Plugins erfasst.

Der Web&shy;auftritt enthält keine Eingabe&shy;felder, in welche die Besuchenden Daten eingeben könnten.

Der Inhalt dieses Web&shy;auftritts wurde von einer Privat&shy;person erstellt. Der Inhalt ist nicht gewerblicher Art.

Für den Inhalt und Daten&shy;schutz verantwortlich:
```
Kai Özer
Albrecht-Achilles-Str. 8
10709 Berlin
Deutschland
Email: hello-961@robo.fish
```

Nehmen Sie über die oben genannten Kontakt&shy;möglichkeiten Verbindung auf, werden Ihre übermittelten Daten nicht an unberechtigte Dritte weiter&shy;gegeben und nur solange gespeichert wie für die Bearbeitung nötig.


<br />

## 2. Datenschutzerklärungen zum Softwareprodukt <i>Belanz</i>

<br />

### 2.1. Datensparsamkeit

Das Software&shy;produkt Belanz erstellt Jahres&shy;abschlüsse aus von den Benutzenden eingegebenen Daten. Ein Jahres&shy;abschluss wird aus allen erhobenen Daten aufgestellt. In Belanz gibt es keine Daten&shy;eingabefelder in denen, für den Jahres&shy;abschluss nicht relevante Daten eingegeben werden können.

<br />

### 2.2. Datenübertragbarkeit und Löschen von Daten

Belanz speichert von den Benutzenden einge&shy;gebene Daten und den er&shy;stellten Jahres&shy;abschluss in von den Benutzenden angegebenen Dateien auf dem Datei&shy;system der Benutzer&shy;geräte. Eine zusätzliche Über&shy;tragung der Daten an eine andere Stelle findet nicht statt. Die Benutzenden haben somit volle Kontrolle über die gespeicherten Daten, können sie kopieren oder löschen.

<br />

### 2.3. Sonstiges

Belanz erhebt keine Statistik über dessen Nutzung, versendet keine Telemetrie&shy;daten und erstellt keine Log-Dateien. Der Quellcode von Belanz und somit die Daten&shy;verarbeitungs&shy;funktionen ist öffentlich einsehbar.

<div id="english" style="margin-bottom: 1em">&nbsp;</div>
<hr/>

# Privacy Statements

<br />

## 1. Privacy statements for this website

The purpose of this privacy statement is to inform visitors of the website <i>belanz.gitlab.io</i> about the methods, scope, and purpose of collecting the personal data of visitors, as well as the use of that collected data.

This website has been generated by the [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) service. The responsibility for the handling of user data is thus divided in the responsibility of the web service provider and the responsibility of the website content provider.

<br />

### 1.1. Privacy statement of the web service provider

The web service that provides visiting web browsers access to the website content (belanz.gitlab.io) is controlled by [GitLab Inc.](https://gitlab.com), where the "website content" is defined as all data transmitted over the HTTP protocol from the Internet address <i>belanz.gitlab.io</i> to the IP address of the visitor, and the "web service" is defined as the hardware and software infrastructure that makes accessing the website content via HTTP protocol possible and also controls the parameters of the data connection. As such, GitLab Inc. has access to the website visitor's IP address, which is considered personal data. The same data is not accessible to the provider of the website content.

The privacy statement and contact addresses of GitLab Inc. can be found at [https://about.gitlab.com/privacy](https://about.gitlab.com/privacy/) and [https://about.gitlab.com/company/visiting](https://about.gitlab.com/company/visiting/), respectively.

<br />

### 1.2. Privacy statement of the website content provider

The website consists of pre-generated static web pages with no program code (JavaScript), meaning that the content loaded into the visitor's browser is not capable of querying the browser for personal data like the IP address. Neither is any data being placed in the browser's storage space (so-called cookies) that leave traces of the visit and could be abused to track the browser's web surfing activity across websites.

The pages of the website do not include links to third-party services that get activated as the pages are loaded and displayed. Links to third-party content or services are clearly marked as such, need to be intentionally clicked on by the user, and will result in leaving the website. In other words, no advertisement services, no analysis tools or social media services get notified of the website visits.

The pages of the website do not contain data input fields for visitors to enter some kind of data.

The website content was prepared by an individual person for non-commercial use.

The directly responsible individual for the website content and related privacy issues:
```
Kai Özer
Albrecht-Achilles-Str. 8
10709 Berlin
Germany
Email: hello-961@robo.fish
```

Any information sent to the contact address or e-mail shown above will be treated confidential, not disclosed to unauthorized third parties, and stored only as long as needed to process the request.

<br />

## 2. Privacy statement for the Belanz application

<br />

### 2.1. Minimal user data

The software application Belanz generates financial statements from data provided by the user. All of the provided data is used for generating a financial statement. There are no data input fields that are not relevant to generating a financial statement.

<br />

### 2.2. Transfer or removal of user data

Belanz stores the generated financial statement and the data provided by the user in user-designated files on the file system of the user's device. The user data is not transferred to any additional location. Users therefore have full control over the stored data: they can transfer or completely delete the stored data via file operations.

<br />

### 2.3. Other

Belanz does not collect usage metrics, does not send usage data to a remote location, and does not generate log files. The source code of Belanz is freely available, allowing users to check in detail how user data is handled.
