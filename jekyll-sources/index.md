---
layout: home
is_test_page: false

description:
  de: "Belanz ist eine Buchhaltungs-App für Kleinst&shy;kapital&shygesell&shyschaften, die aus dem Eigenbedarf einer Ein-Mann-GmbH entstanden ist. Belanz unter&shy;stützt die doppelte Buch&shy;führung und erstellt Jahres&shy;abschlüsse."
  en: "Belanz is a software application for micro-corporations in Germany. The main features are double-entry book&shy;keeping and finan&shy;cial state&shy;ments. The docu&shy;mentation is mostly in German."

download:
  target_platforms: "für macOS 13 (Ventura) oder neuer"

get_started:
  de: "Kurzanleitung"
  en: "Get Started"

feature_1:
  title: "Firmendaten eintragen"
  text: "Für jedes Geschäfts&shy;jahr erstellen Sie ein Belanz-Dokument und tragen als Erstes die Unter&shy;nehmens&shy;daten ein, inklu&shy;sive Anlagen und Ver&shy;bind&shy;lich&shy;keiten."

feature_2:
  title: "doppelt Buch führen"
  text: "Sie können in Belanz nach doppelter Buchführung kontieren. Zur Verfügung stehen die Kontenrahmen
	<span style=\"white-space: nowrap;\">SKR 03</span>, IKR oder DE-GAAP."

feature_3:
  title: "Jahres&shy;abschluss erstellen"
  text: "Der erstellte Jahres&shy;abschluss wird als Teil des Belanz-Dokuments ge&shy;spei&shy;chert und bildet die Basis für das nächste Geschäfts&shy;jahr."

feature_4:
  title: "… und exportieren"
  text: "Den Jahres&shy;abschluss können Sie als LaTeX- oder <a class=\"belanz_primary_1\" href=\"https://www.myebilanz.de\">myebilanz</a>-INI-Datei exportieren. Mit den Dateien erstellen Sie in anderen Apps ein PDF, bzw. senden die Daten digital an die Steuerbehörde."

open_source: "Offener Quellcode / Open Source"
---
